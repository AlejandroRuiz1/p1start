package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;


/**
 * 
 * @author alejandroruiz
 * 
 * Rest API used in this project.
 *
 */

@Path("/cars")
public class CarManager{

	/**
	 * @return Car[] - Returns a list of all the cars in the CSDLL as a Car[].
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		// Instantiate carList
		SortedList<Car> carList = CarList.getInstance();
		// If carList is empty return an empty Car[]
		if(carList.isEmpty()) {
			return new Car[1];
		}
		else {
			// Create Car[] result that has same size as carList 
			Car[] result = new Car[carList.size()];
			// For all cars in carList, add them to the Car[] result
			for (int i = 0; i < carList.size(); i++) {
				result[i] = carList.get(i);
			}
			// Return the Car[] of all cars 
			return result;
		}
	}

	/**
	 * 
	 * @param id - id of car to be returned 
	 * @return - car of specified id
	 * 
	 * This method looks for the car in the list with the specified carId and returns it.
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		// Instantiate carList
		SortedList<Car> carList = CarList.getInstance();
		// For each car in the carList
		for (Car c : carList) {
			// If the car we are looking for, with specified carId, has been found, then return it.
			if(c.getCarId() == id) {
				return c;
			}
		}
		// Else throw 404 error, Car not found!
		throw new WebApplicationException(404);
	}

	/**
	 * 
	 * @param newCar - car to be added to the carList
	 * @return Response - Car has been added.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar) {
		// Instantiate carList
		SortedList<Car> carList = CarList.getInstance();
		// Add newCar to the carList
		carList.add(newCar);
		// Return a response saying it was added
		return Response.status(201).build();
	}
	
	/**
	 * 
	 * @param updatedCar - car to be updated in the carList
	 * @return Response - Car was successfully updated or not.
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car updatedCar) {
		// Instantiate carList
		SortedList<Car> carList = CarList.getInstance();
		// For each car in carList
		for (Car c : carList) {
			// If car has been found
			if(c.getCarId() == updatedCar.getCarId()) {
				// Remove previous version of the car
				carList.remove(c);
				// Add the new version of the car
				carList.add(updatedCar);
				// Return response saying it was added (updated)
				return Response.status(Response.Status.OK).build();
			}
		}
		// Car was not updated
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * 
	 * @param id - id of car to be removed from carList
	 * @return Response - Car was removed or not.
	 */
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		// Instantiate carList
		SortedList<Car> carList = CarList.getInstance();
		// For each car in carList
		for(Car c: carList) {
			// If car has been found
			if(c.getCarId() == id) {
				// Remove the car
				carList.remove(c);
				// Return response saying it was successfully removed
				return Response.status(Response.Status.OK).build();
			}
		}
		// Car was not removed
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
