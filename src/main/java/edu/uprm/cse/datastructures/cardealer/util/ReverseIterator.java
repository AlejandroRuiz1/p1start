package edu.uprm.cse.datastructures.cardealer.util;

/**
 * 
 * @author alejandroruiz
 * 
 * ReverseIterator Interface that specifies the methods for an iterator that traverses List in reverse direction.
 *
 * @param <E>
 */
public interface ReverseIterator<E> {

/**
 * Verifies if the list has an element previous to the current element
 * 
 * @return - True if there is an element previous to the current element, false if not.
 */
public boolean hasPrevious();

/**
 * Moves the iterator to the element previous to the current element, returns element.
 * 
 * @return - element previous to the current element
 */
public E previous();

}
