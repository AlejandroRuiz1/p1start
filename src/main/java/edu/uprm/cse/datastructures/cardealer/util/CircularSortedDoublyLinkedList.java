package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 
 * @author alejandroruiz
 *
 * @param <E>
 * 
 * CSDLL Data Structure used in this project.
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	/**
	 * 
	 * Node class to be used in the CSDLL
	 *
	 */
	private static class Node<E>{
		
		// Instance variables 
		private E element;
		private Node<E> next;
		private Node<E> prev;

		// Default Constructor
		public Node(){
			super();
		}

		// Constructs a Node with a specified element, and references to next and prev
		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		
		// Getters and Setters for Node Class

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	// Default Forward Iterator 
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> current;

		// Default Constructor
		public CircularSortedDoublyLinkedListIterator() {
			this.current = (Node<E>) header.getNext();
		}

		//  Constructs an iterator that starts at a specified index
		public CircularSortedDoublyLinkedListIterator(int index) {
			// Check if index is valid
			if(index < 0 || index > currentSize) {
				throw new IndexOutOfBoundsException();
			}
			// Cursor Node
			Node<E> temp = (Node<E>) header.getNext();
			// Find Node at specified index
			for(int i = 0; i < index; i++) {
				temp = temp.getNext();
			}
			this.current = temp;
		}

		@Override
		public boolean hasNext() {
			return current.getElement() != null;
		}

		/**
		 *  Stores current element in temp variable,
		 *  sets current node to the next node and returns
		 *  result.
		 */
		@Override
		public E next() {
			if (this.hasNext()) {
				E result = current.getElement();
				current = current.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}

	/**
	 * Implementation of the Reverse Iterator using the ReverseIterator interface.
	 */
	private class CircularSortedDoublyLinkedListReverseIterator<E> implements ReverseIterator<E>{

		private Node<E> current;

		// Default Constructor for ReverseIterator
		public CircularSortedDoublyLinkedListReverseIterator() {
			this.current = (Node<E>) header.getPrev();
		}

		// Constructs a Reverse Iterator starting at a specified index
		public CircularSortedDoublyLinkedListReverseIterator(int index) {
			if(index < 0 || index > currentSize) {
				throw new IndexOutOfBoundsException();
			}

			// Cursor Node
			Node<E> temp = (Node<E>) header.getNext();

			// Seek node at specified index
			for(int i = 0; i < index; i++) {
				temp = temp.getNext();
			}
			this.current = temp;
		}

		@Override
		public boolean hasPrevious() {
			return current != header;
		}

		// Stores value of current, sets current to prev, returns stored element
		@Override
		public E previous() {
			if(this.hasPrevious()) {
				E result = this.current.getElement();
				this.current = this.current.getPrev();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}
	
	// Instance Variables used in the CSDLL class
	private Node<E> header;
	private int currentSize;
	private Comparator<E> cmp;

	/**
	 *  Default Constructor, constructs an empty CSDLL
	 *  header next points to header, header prev points
	 *  to header. currentSize = 0
	 */
	public CircularSortedDoublyLinkedList() {
		header = new Node<E>(); 
		this.header.setNext(header);
		this.header.setPrev(header);
		this.currentSize = 0;
	}

	/**
	 * 
	 * @param c - Takes a Comparator as a parameter.
	 * Constructs header node, sets next and prev to itself,
	 * sets cmp to the Comparator passed as a parameter.
	 * 
	 */
	public CircularSortedDoublyLinkedList(Comparator<E> c){
		this.header = new Node<E>();
		this.header.setNext(header);
		this.header.setPrev(header);
		this.currentSize = 0;
		this.cmp = c;
	}

	/**
	 * 'Add' method which uses a comparator to sort the objects added
	 * to the CSDLL. When using the comparator, e is added according
	 * to the value obtained by using cmp.compare. This way the items 
	 * added to the CSDLL are sorted.
	 * 
	 * @param E e - Object to be added.
	 */
	@Override
	public boolean add(E e) {
		// If e is null, e can't be added and an IllegalArgExcep is thrown.
		if(e.equals(null)) {
			throw new IllegalArgumentException("Argument can't be null");
		}

		// Boolean variable that makes sure e has been added.
		boolean added = false;

		/**
		 *  First Case: List is empty
		 *  
		 *  Initialize a newNode with e as the element, header as next and prev.
		 *  E has been added so added is updated to true. Size is increased.
		 */
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(e, header, header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			added = true;
			currentSize++;
			return added;
		}
		// Cursor Node
		Node<E> target = this.header.getNext();

		// While the whole list hasn't been traversed:
		while(!target.equals(header)) {
			// If e comes before target, add before target.
			if(cmp.compare(e, target.getElement()) < 0) {
				Node<E> newNode = new Node<E>(e, target, target.getPrev());
				target.getPrev().setNext(newNode);
				target.setPrev(newNode);
				added = true;
				currentSize++;
				break;
			}
			// e comes after target, get node after target and check if e comes before target
			target = target.getNext();
		}
		// If e hasn't been added yet, it must be added at the end of the list.
		if(added == false) {
			Node<E> newNode = new Node<E>(e, header, header.getPrev());
			header.getPrev().setNext(newNode);
			header.setPrev(newNode);
			added = true;
			currentSize++;
		}
		// Return if added or not
		return added;
	}

	// Returns Size of the List
	@Override
	public int size() {
		return this.currentSize;
	}

	// Check if e is in the list, if it is, remove using remove(index) method
	@Override
	public boolean remove(E e) {
		// this.firstIndex(E) returns -1 if e not found, otherwise it returns index of the first instance of e.
		int index = this.firstIndex(e);
		// If e not found, can't remove it so return false
		if(index < 0) {
			return false;
		}
		else {
			return this.remove(index);
		}
	}

	// Removes object in the position 'index' of list
	@Override
	public boolean remove(int index) {
		// Check if index is valid
		if((index < 0) || (index > this.currentSize))
			throw new IndexOutOfBoundsException();

		// Helper variables.
		int i = 0;
		Node<E> target = header.getNext();

		// While the whole list hasn't been traversed..
		while(!target.equals(header)){
			// If i has reached the specified index
			if(index == i){
				target.getPrev().setNext(target.getNext());
				target.getNext().setPrev(target.getPrev());
				target.setElement(null);
				target.setPrev(null);
				target.setNext(null);
				currentSize--;
				return true;
			}
			// Keep searching for the next index, i++
			target = target.getNext();
			i++;
		}
		return false;
	}

	// Removes all instances of e in the list using the remove(index) method.
	@Override
	public int removeAll(E e) {
		int count = 0;
		while(this.remove(e)) {
			count++;
		}
		return count;
	}

	// Returns element of the first object in the list 
	@Override
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getNext().getElement();
	}

	// Returns element of the last element in the list 
	@Override
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getPrev().getElement();
	}

	// Returns element of object at specified index.
	@Override
	public E get(int index) {
		// Check if input is valid
		if(index < 0 || index > this.size()-1) {
			throw new IndexOutOfBoundsException("Illegal Index");
		}
		// Cursor Node
		Node<E> result = this.header;
		// Find node previous to specifed index (index - 1)
		for (int i = 0; i < index; i++) {
			result = result.getNext();
		}
		// Returns element at index
		return result.getNext().getElement();
	}

	// Clears List by removing first element in list until list is empty.
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(this.first());
		}
	}

	// Returns true if the list contains e 
	@Override
	public boolean contains(E e) {
		// if firstIndex returns a valid index then the list contains e
		if(this.firstIndex(e) != -1) {
			return true;
		}
		// e is not in the list
		return false;
	}

	// Returns true if list is empty, false otherwise
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	// Returns the index of the first instance of e in the list 
	@Override
	public int firstIndex(E e) {
		Node<E> current = this.header.getNext();
		int index = 0;
		// Traverses the entire list looking for e
		while(!current.equals(header)) {
			if(current.getElement().equals(e)) {
				return index;
			}
			current = current.getNext();
			index++;
		}
		// e is not in the list
		return -1;
	}

	// Returns the index of the last instance of e in the list
	@Override
	public int lastIndex(E e) {
		Node<E> current = this.header.getNext();
		int index = -1;
		int i = 0;
		// Traverses the list looking for e
		while(!current.equals(header)) {
			// If e has been found save the index, but continue looking for another instance of e
			if(current.getElement().equals(e)) {
				index = i;
			}
			current = current.getNext();
			i++;
		}
		// Returns last instance of e
		return index;
	}

	// Returns CSDLL Forward iterator
	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	// Returns CSDLL Forward Iterator beginning at a specified index
	@Override
	public Iterator<E> iterator(int index) {
		return new CircularSortedDoublyLinkedListIterator<E>(index);
	}

	// Returns CSDLL Reverse iterator "Backwards iterator"
	@Override
	public ReverseIterator<E> reverseIterator() {
		return new CircularSortedDoublyLinkedListReverseIterator<E>();
	}

	// Returns CSDLL Reverse Iterator beginning at a specified index 
	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		return new CircularSortedDoublyLinkedListReverseIterator<E>(index);
	}

}
