package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;


/**
 * 
 * @author alejandroruiz
 * 
 * CarComparator class which compares Cars by concatenating c1's and c2's carBrand,
 * carModel, and CarModelOption to a string and then comparing them to each other.
 *
 */
public class CarComparator implements Comparator<Car>{

	/**
	 * @return - 0 if cars are equal
	 * 			 1 if c1 is "greater" than c2, c1 goes after c2.
	 * 			-1 if c1 is "smaller" than c2, c1 goes before c2.
	 */
	@Override
	public int compare(Car c1, Car c2) {
		String car1 = c1.getCarBrand() + c1.getCarModel() + c1.getCarModelOption();
		String car2 = c2.getCarBrand() + c2.getCarModel() + c2.getCarModelOption();
		return car1.compareTo(car2);
	}

}
