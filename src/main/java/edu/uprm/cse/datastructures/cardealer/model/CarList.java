package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {

	// Create instance of CSDLL to be used
	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	// Constructor 
	private CarList() {
		carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		}
	
	// Returns instance of the carList
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}

	// Needed method for APITest to work, clears the carList
	public static void resetCars() {
		carList.clear();
	}

}
